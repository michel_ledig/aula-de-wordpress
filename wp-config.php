<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'W911NChvXcF34769orV6hzyMO9tOCndPzJkOs6ERBWSSkjBXVmsNGTqiqRwtrYDp7cy+oCHPbBtkopM/AsXcGQ==');
define('SECURE_AUTH_KEY',  'mFkwWBU4MAZEwyaUcZMMdoJe6WIqwQYue6kiQYBI7MrTO3o8WKk965OIN6BdSB5hLU5hg/oS8osRwSE3y3m70g==');
define('LOGGED_IN_KEY',    'pmuZCjANfPYz+no9LaPAn97JvcsC+U/7FJ/PQzmYIz6ZAIUGs704OGXObSuy0XhZyc8SA9h1lTX1eDQQV6/rvw==');
define('NONCE_KEY',        'hIGuOwiiuWDtfmh7cxKyfnJ/kz9scq6bkoEPlGRXXjSZN/CsZPFFp/yTiS9yIGHRpNRoVzfVoPPHYNGDRTN3xQ==');
define('AUTH_SALT',        'IXtQyVwE1Y1lGrI8Noa3VAs6qJsxtwuRjaYJL4XO8vDJrCZnha0UIoc4Bfck1SH4Wr/ZBtdqtXcs92D/bpSrGg==');
define('SECURE_AUTH_SALT', 'ZFT5jNF+AhGg7Ei+UQEn68PW45/el1mDpjZf8mZBLbbikoc43WPxhdM2ldrfeuNwNJffTQ4TgfcIH/L6DTBsDw==');
define('LOGGED_IN_SALT',   'JSs1Ug26hoxY6hUvLJKHIDTjsGvvW6cVhCeaGrBjeu02520GXjc4tx2keQDnYOVsvuq4NGtE7l2JDOFCC3/q1Q==');
define('NONCE_SALT',       '0ZsVkV3K3cJ3bHiLuX7Xn52nYCvZxvbC3bUxxMxTl58JdUqyDxi5jmiFHNp8aKeWWFHKplUVj3W7tCh5qpR/Aw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
